<?php
class Level extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('level_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
       
    }
 
    public function index()
    {

        $data['error'] = '';
       
        $this->load->view('template/header');
        $this->load->view('level/index');
        $this->load->view('template/footer');
    }


  

}