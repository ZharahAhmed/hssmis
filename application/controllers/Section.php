<?php
class Section extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('section_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('session');
       
    }
 
    public function index()
    {

        $data['error'] = '';
       
        $this->load->view('template/header');
        $this->load->view('section/index');
        $this->load->view('template/footer');
    }


  

}